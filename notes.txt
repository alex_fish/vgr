What to return in search results
--------------------------------
-game name
-platforms
-average rating
-thumbnail image


query model
api_key = 'b13a7ce3a2cbfe16421330e0ef920bf5bbe8bff0'
base_url = 'http://www.giantbomb.com/api/search/?api_key=' + api_key

-Recommendation functionality
-----------------------------

-if critical mass
	-use collaborative filtering

else if content-based filtering implemented
	-use that

else
	-use list of most similar games





Content-Based Filtering
========================
-create "preference vector" table
	-for each user, maintain string or vector associated with their average preferences for games
		-store this vector as a comma separated integer field or string
	-each time a user rates an unrated game or changes a rating, update the vector
	-also store average release date

-to predict how user u will rate game i:
	-compute the genre vector V_i for i
	-compute the cosine similarity between v_i and v_u
	-compute x = 1 - cosine_sim(v_i, v_u)
	-compute y = distance (in days) between average release date and i's first release date
	-use cosine simliarity and release date to compute score

-to make an ordered list of recommedations for user u
	-pick a set S of games
	-for each game  i in S
		-predict how u will rate
	-sort S based on these predictions
	-return a subset of this sorted list as a list of recommendations

-to compute set S to compute predictions for
	-for k top rated games in u's library
		-pick n simillar games to top rated game, add these to S
		-pick 2 games from the same franchise as k, add these to S
========================


Evaluation data
===============
-collect information when user submits HTTP post from recommendation results page
	-if user dislikes/hides game
	-if user likes/rates game
===============




Collaborative Filtering
=======================
-make table of (game, game) = (i, j) similarities
	-store sum(r_adj(u, i) * r_adj(u, j))
	-store sum(square(r_adj(u, i))
	-store sum(square(r_adj(u, j))
	------------------------------
	-storing ^these values will make it easy to compute similarity incrementally

-add in most similar game columns to game table
	-when sim(i, j) is updated, update most similar games for game i and game j


-make table of average user ratings

-sim = 
		sum(r_adj(u, i) * r_adj(u, j)) of users u that have rated i and j
		-------------------------------
		sqrt(sum(r_adj(u, i)^2) * sum(r_adj(u, j)^2) of users u that have rated i and j)
-let i be a game that u has rated before, and j be a new game to rate
	-update sim(i, j) for all i that user i has rated before
	-new gamee

