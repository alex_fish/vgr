from django.db import models
from django.contrib.auth.models import User

NUM_GENRES = 48



class Genre(models.Model):
	#id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=30)

	def __unicode__(self):
		return self.name

# Get other game info by looking up ID on Metacritic or Giantbomb
class Game(models.Model):
	db_id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=40)
	genres = models.ManyToManyField(Genre)
	detail = models.URLField()

	def __unicode__(self):
		return self.name

class Rating(models.Model):
	game = models.ForeignKey(Game)
	user = models.ForeignKey(User)
	score = models.IntegerField()

	class Meta:
		unique_together= ("game", "user")

	def __unicode__(self):
		return u'%s : %s - %s' % (self.user.username, str(self.score), self.game.name)

# a user's preference for a particular genre
class Preference(models.Model):
	user = models.ForeignKey(User)
	value = models.FloatField()
	genre = models.ForeignKey(Genre)

	class Meta:
		unique_together= ("user", "genre")

	def __unicode__(self):
		return u'%s : %s' % (self.user.username, self.genre.name)

# games that the user has not played
class Wishlist(models.Model):
	user = models.OneToOneField(User)
	games = models.ManyToManyField(Game)

	def __unicode__(self):
		return self.user.username

class Evaluation(models.Model):
	user = models.ForeignKey(User)
	numResults = models.IntegerField()
	numRelevantResults = models.IntegerField()

	def __unicode__(self):
		return self.user.username

