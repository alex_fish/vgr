from django.contrib import admin
from recommender.models import Game, Rating, Genre, Preference, Wishlist, Evaluation
admin.site.register(Game)
admin.site.register(Rating)
admin.site.register(Genre)
admin.site.register(Preference)
admin.site.register(Wishlist)
admin.site.register(Evaluation)
