from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from recommender.models import Game, Rating, Genre, Preference, Wishlist, Evaluation
from giantbomb import giantbomb

GIANTBOMB_API_KEY = 'b13a7ce3a2cbfe16421330e0ef920bf5bbe8bff0'



@login_required
def index(request):
	c = {}
	return render_to_response('index.html', c,
							  context_instance=RequestContext(request))

@login_required
def wishlist(request):
	return render_to_response('wishlist.html', context_instance=RequestContext(request))


def register_view(request):
	c = {}
	return render_to_response('register.html', c,
						  	  context_instance=RequestContext(request))

def process_register(request):
	c = {}
	post = request.POST.copy()
	try:
		user = User.objects.get(username__exact=post['username'])
	except User.DoesNotExist:
		a = User.objects.create_user(username=post['username'], password=post['password'])
		return redirect('/')
	# return a duplicate username error message

def login_view(request):
	c ={}
	return render_to_response('login.html', c,
							  context_instance=RequestContext(request))

# code adapted from https://docs.djangoproject.com/en/dev/topics/auth/default/
def process_login(request):
	post = request.POST.copy()
	user = authenticate(username=post['username'], password=post['password'])
	if user is not None:
		if user.is_active:
			login(request, user)
			return redirect('/')
	return redirect('/')

def logout_view(request):
	logout(request)
	c = {}
	return redirect('/')


@login_required
def search_view(request):
	return render_to_response('search.html', context_instance=RequestContext(request))

@login_required
def search_results(request):
	c = {}
	if (request.method == 'GET'):
		get = request.GET.copy()
		gb = giantbomb.Api(GIANTBOMB_API_KEY)
		games = gb.search(query=get['game'])
		ratings = Rating.objects.filter(user=request.user)

		c = {'games':games}
	return render_to_response('search_results.html', c, context_instance=RequestContext(request))


# compute recommendation score for given game and user
def computeScore(user, gameId):
	ratings = Rating.objects.filter(user=user)
	n = ratings.exclude(score=-1).count() # number of ratings user has made
	gb = giantbomb.Api(GIANTBOMB_API_KEY)
	genres = gb.getGameGenres(gameId)
	if genres == None:
		return 0

	score = 0
	for genre in genres:
		try:
			genreDb = Genre.objects.get(name=genre.name)
		except Genre.DoesNotExist:
			continue
		try:
			pref = Preference.objects.get(user=user, genre=genreDb)
			prefVal = pref.value / n
		except Preference.DoesNotExist:
			continue
		score += prefVal
	return score

# get list of games to recommend for user u
def getRecommendList(user):
	# pick out top 10 rated games by user
	# for each of these games:
	      # add in 10 of the similar games that the user hasn't rated
	      # add in the franchise games that the user hasn't rated

	ret = []
	ratings = list(Rating.objects.filter(user=user).order_by('-score'))
	gb = giantbomb.Api(GIANTBOMB_API_KEY)
	ratingsLen = min(10, len(ratings))
	for i in range(0, ratingsLen - 1):
		simGames = gb.getSimilarGames(ratings[i].game.db_id)
		simLen = min(10, len(simGames))
		for j in range(0, simLen - 1):
			# only include games that haven't been seen by user
			try:
				Rating.objects.get(user=user, game__db_id=simGames[j].id)
			except Rating.DoesNotExist:
				game = Game(db_id=simGames[j].id, name=simGames[j].name, detail=simGames[j].site_detail_url)
				ret.append(game)

				#ret.append(simGames[j])
			j += 1
		i += 1
	return ret


@login_required
def recommend(request):
	return render_to_response('recommend.html', context_instance=RequestContext(request))


@login_required
def recommend_games(request):
	recommended = getRecommendList(request.user)
	#scoreList = []
	gameScores = {}
	for game in recommended:
		#scoreList.append((game, computeScore(request.user, game.id)))
		gameScores[game] = computeScore(request.user, game.db_id)
	scoreList = sorted(gameScores.items(), key = lambda x : x[1], reverse=True)
	c = {'recommendedGames' : scoreList}
	return render_to_response('recommended_games.html', c, context_instance=RequestContext(request))


# create game with GiantBomb id game_id and add it to the DB
def create_game(game_id):
	gb = giantbomb.Api(GIANTBOMB_API_KEY)
	gameInfo = gb.getGame(int(game_id))
	game = Game(db_id=game_id, name=gameInfo.name, detail=gameInfo.site_detail_url)
	game.save()

	# get genres associated with the game
	for gameGenre in gameInfo.genres:
		try:
			genre = Genre.objects.get(id=gameGenre.id)
		except Genre.DoesNotExist:
			genre = Genre(id=gameGenre.id, name=gameGenre.name)
			genre.save()
		game.genres.add(genre)
	game.save()
	return game


def add_to_lib(game_id, user):
	try:
		game = Game.objects.get(db_id=int(game_id))
	except Game.DoesNotExist:
		game = create_game(int(game_id))
		
	try:
		rating = Rating.objects.get(game=game, user=user)
	except Rating.DoesNotExist:
		rating = Rating(game=game, user=user, score=-1)
		rating.save()

def add_to_wishlist(game_id, user):
	try:
		game = Game.objects.get(db_id=int(game_id))
	except Game.DoesNotExist:
		game = create_game(int(game_id))
		
	try:
		wishlist= Wishlist.objects.get(user=user)
	except Wishlist.DoesNotExist:
		wishlist = Wishlist(user=user)
		wishlist.save()
	wishlist.games.add(game)
	wishlist.save()

@login_required
def add_games(request):
	c = {}
	if (request.method == 'POST'):
		post = request.POST.copy()
		for id, option in post.iteritems():
			if option == 'none':
				continue
			elif option == 'lib':
				add_to_lib(int(id), request.user)
			elif option == 'wish':
				add_to_wishlist(int(id), request.user)

		# if (post.has_key('games')):
		# 	gamesList = post.getlist('games')
		# 	for game_id in gamesList:
		# 		add_to_lib(game_id, request.user)
				# if game with gameId doesn't exist, add it to db
				
	return redirect('/')

# remove game from user's list of ratings and update average ratings and similarities accordingly
def remove_game(user, game):
	try:
		rating = Rating.objects.get(user=user, game=game)
	except Rating.DoesNotExist:	
		return

	if rating.score == -1:
		rating.delete()
		return

	# update user preferences
	for genre in game.genres.all():
		try:
			preference = Preference.objects.get(user=user, genre=genre)
		except Preference.DoesNotExist:
			continue
		preference.value -= (rating.score - 3)
		preference.save()

	rating.delete()

# update user's rating of game with score and update average ratings and similarities accordingly
def rate_game(user, game, score):

	# update rating
	rating = Rating.objects.get(game=game, user=user)
	old_score = rating.score
	rating.score = score
	rating.save()

	# update user preferences
	for genre in game.genres.all():
		try:
			preference = Preference.objects.get(user=user, genre=genre)
		except Preference.DoesNotExist:
			preference = Preference(user=user, genre=genre, value=0)
		if old_score == -1:
			preference.value += (rating.score - 3)
		else:
			preference.value += (rating.score -3) - (old_score - 3)
		preference.save()



@login_required
def manage_game(request):
	c = {}
	if (request.method == 'POST'):
		post = request.POST.copy()
		if post.has_key('remove_game'):
			try:
				game = Game.objects.get(db_id=post['gameId'])
			except Game.DoesNotExist:
				return redirect('/')
			remove_game(request.user, game)
			return redirect('/')

		elif post.has_key('rate_game'):
			try:
				game = Game.objects.get(db_id=post['gameId'])
			except Game.DoesNoteExist:
				return redirect('/')
			rate_game(request.user, game, int(post['score']))
			return redirect('/')
		else:
			return redirect('/')
	return redirect('/')

@login_required
def manage_wishlist(request):
	if (request.method == 'POST'):
		post = request.POST.copy()
		try:
			wishlist = Wishlist.objects.get(user=request.user)
		except Wishlist.DoesNotExist:
			return redirect('/wishlist')

		try:
			game = Game.objects.get(db_id=post['gameId'])
		except Game.DoesNotExist:
			return redirect('/wishlist')
			

		if post.has_key('remove_game'):
			wishlist.games.remove(game)
			wishlist.save()
			return redirect('/wishlist')

		elif post.has_key('add_to_lib'):
			add_to_lib(post['gameId'], request.user)
			wishlist.games.remove(game)
			wishlist.save()
			return redirect('/wishlist')
		else:
			return redirect('/wishlist')
	return redirect('/wishlist')


@login_required
def manage_recommended(request):
	if request.method == 'POST':
		post = request.POST.copy()
		numResults = 0
		numRelevantResults = 0
		for id, option in post.iteritems():
			if option == 'none':
				numResults += 1
				continue
			elif option == 'lib':
				numResults += 1
				numRelevantResults += 1
				add_to_lib(int(id), request.user)
			elif option == 'wish':
				numResults += 1
				numRelevantResults += 1
				add_to_wishlist(int(id), request.user)
		evaluation = Evaluation(user=request.user, numResults=numResults, numRelevantResults=numRelevantResults)
		evaluation.save()


	return redirect('/')
