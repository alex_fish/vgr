from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', 'recommender.views.index'),
	url(r'^register$', 'recommender.views.register_view'),
    url(r'^register_submit$', 'recommender.views.process_register'),
	url(r'^login$', 'recommender.views.login_view'),
	url(r'^logout$', 'recommender.views.logout_view'),
    url(r'^login_submit$', 'recommender.views.process_login'),
    url(r'^search$', 'recommender.views.search_view'),
    url(r'^search_results$', 'recommender.views.search_results'),
    url(r'^recommend$', 'recommender.views.recommend'),
    url(r'^recommend_games$', 'recommender.views.recommend_games'),
    url(r'^add_games$', 'recommender.views.add_games'),
    url(r'^manage_game$', 'recommender.views.manage_game'),
    url(r'^manage_recommended$', 'recommender.views.manage_recommended'),
    url(r'^wishlist$', 'recommender.views.wishlist'),
    url(r'^manage_wishlist$', 'recommender.views.manage_wishlist'),
    
    # Examples:
    # url(r'^$', 'vgr.views.home', name='home'),
    # url(r'^vgr/', include('vgr.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

